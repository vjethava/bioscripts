"""MinimumSpanningTree.py

Kruskal's algorithm for minimum spanning trees. D. Eppstein, April 2006.

-- Adapted by Vinay Jethava, 2013. 

"""

from UnionFind import UnionFind

def MinimumSpanningTree(G, n=None):
    """
    Return the minimum spanning tree of an undirected graph G.
    G should be represented in such a way that G[u][v] gives the
    length of edge u,v, and G[u][v] should always equal G[v][u].
    The tree is returned as a list of edges.
    """
    # Kruskal's algorithm: sort edges by weight, and add them one at a time.
    # We use Kruskal's algorithm, first because it is very simple to
    # implement once UnionFind exists, and second, because the only slow
    # part (the sort) is sped up by being built in to Python.
    if n is None: 
        n = len(G) 
    subtrees = UnionFind()
    tree = []
    edges = [(G[u][v],u,v) for u in range(n) for v in range(n)]
    edges.sort() # sort of first item of tuple in ascending order 
    # print edges 
    num_edges = 0 
    for W,u,v in edges:
        # keep merging subtrees till can't find edges or all relevant edges have been added. 
        if subtrees[u] != subtrees[v]:
            tree.append((u,v))
            subtrees.union(u,v)
            num_edges = num_edges + 1 
        # print count, num_edges 
        # count = count + 1   
        if num_edges == (n-1): 
            # all relevant edges added
            break 
    return tree        
