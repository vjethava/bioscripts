# logger.py --- 
# 
# Description: Logger for bioscripts/ project. Code refactored from extract_common/ 
# 
# Status: 
# Author: Vinay Jethava
# Created: Thu Jan 16 21:23:07 2014 (+0100)
# Last-Updated: 
#           By: 
#     Update #: 0
# 

# Change Log:
# 
# 

# Code:


import logging 


# standard logging 
logger = logging.getLogger('extract_common.log') 
loggerLevel = logging.INFO
logger.setLevel(loggerLevel)  
channel = logging.StreamHandler()
channel.setLevel(loggerLevel)

verbose_formatter = logging.Formatter('\n%(asctime)s -  %(levelname)s -[%(filename)s:%(lineno)s - %(funcName)s()]\n\t%(message)s')
basic_formatter = logging.Formatter('\n%(asctime)s - %(message)s')
channel.setFormatter(basic_formatter)
logger.addHandler(channel) 

def set_verbosity(verbosity): 
    if verbosity == 2: 
        channel.setFormatter(verbose_formatter)         
        logger.setLevel(logging.DEBUG)
    elif verbosity == 0:
        logger.setLevel(logging.ERROR) 
