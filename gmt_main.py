#!/usr/bin/env python 
# 
# USAGE: gmt_main.py -i INPUT_FILE -t THRESHOLD 
#
# Filename: gmt_main.py
#
#  Description: Main file for generating required data. 
# 
# Author: Vinay Jethava
# Created: Mon Nov  4 15:22:41 2013 (+0100)
# Version: 
# Last-Updated: Mon Nov  4 16:10:15 2013 (+0100)
#           By: Vinay Jethava
#     Update #: 38
# 

# Change Log:
# 4-Nov-2013    Vinay Jethava  
#    Last-Updated: Mon Nov  4 15:44:04 2013 (+0100) #28 (Vinay Jethava)
#    Simplified version of gmt-reader. 
# 
# 
# 

############################################################ 
# Global parameters - change these as required
############################################################
OUT_SUFFIX = '_gsID.csv' # output file suffix 
ADJ_SUFFIX = '_adj_list.csv' # adjacency list file suffix
OVERLAP_SUFFIX = '_overlap.csv' # overlap file suffix

# this option adds additional edges to the adjacency list so
# as to obtain a connected graph (computes maximum spanning tree)
GET_CONNECTED_ADJACENCY = True 

# Default threshold if not specified
DEFAULT_THRESHOLD = 0.5 

############################################################
# Main code begins here
############################################################ 
import os, sys, getopt
from gene_matrix_transposed import GeneMatrixTransposed

if __name__=='__main__': 
    input_file = 'example_gmt/example.gmt'
    threshold = DEFAULT_THRESHOLD
    try: 
        opts, args = getopt.getopt(sys.argv[1:], "i:t:", ["input_file=",  "threshold="])
    except getopt.GetoptError:
        print 'gmt_main -i <INPUT_FILE> -o <OUTPUT_FILE> -c <OVERLAP_FILE>'
        sys.exit(2)
    for opt,arg in opts:
        if opt == '-h':
            print 'python gmt_main -i <INPUT_FILE> -t <THRESHOLD>'
            sys.exit()
        elif opt in ('-i', '--input_file'):
            input_file = arg         
        elif opt in ('-t', '--threshold'):
            threshold = arg        

    file_name = input_file.split(".")[0]  
    output_file = file_name + OUT_SUFFIX
    overlap_file = file_name + OVERLAP_SUFFIX
    adj_file = file_name + ADJ_SUFFIX

    print >> sys.stderr, 'Input file: ', input_file, 'threshold: ', threshold
  
    gmt = GeneMatrixTransposed.read_gmt_file(input_file)
    gmt.write_output_file(output_file)
    gmt.write_overlap_file(overlap_file)
    gmt.write_adjacency_list(adj_file, threshold, GET_CONNECTED_ADJACENCY)
    
    
