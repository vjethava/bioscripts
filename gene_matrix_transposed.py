#!/usr/bin/env python 
#
# Description: Reads GMT format and computes overlap between gene sets. 
# 
# 
# Let S_a and S_b be the set of genes in the two gene_sets. 
# Then, the overlap is computed as: 
# 
# overlap(a, b) = intersection(S_a, S_b) / union(S_a, S_b) 
# 
# All edges (a, b) with overlap >= threshold are output to adjacency list.
# 
# To get connected graph, we use Maximum Spanning Tree
# (even if included edges are below the threshold)
#
# Author: Vinay Jethava
# Created: Thu Oct 31 18:19:58 2013 (+0100)
# Version: 
# Last-Updated: Mon Nov  4 18:11:53 2013 (+0100)
#           By: Vinay Jethava
#     Update #: 167
# 
# Change Log:
# 31-Oct-2013    Vinay Jethava  
#    Initial version refactored from gs_reader.py 
# 
# 
# 
import sys 

class GeneSet(set):
    def __init__(self, gene_set=(), name=None, description=None):
        super(GeneSet, self).__init__(gene_set)
        if name is None and hasattr(gene_set, 'name'):
            name = gene_set.name
        if description is None and hasattr(gene_set, 'description'):
            description = gene_set.description
        self.name = name
        self.description = description
    
    @classmethod 
    def parse_gene_set_str(cls, gene_set_str):
        groups = gene_set_str.split('\t') 
        result = GeneSet(groups[2:], groups[0], groups[1]) 
        return result

    def __repr__(self): 
        return super(GeneSet, self).__repr__()
    
    def __str__(self): 
        return '\t'.join([self.name, self.description] + list(self))

    def compute_overlap(self, other_gene_set): 
        if len(self) > 0 and len(other_gene_set) > 0: 
            return  len(self & other_gene_set) / float( len( self | other_gene_set) ) 
        else:
            return 0.0 
    
class GeneMatrixTransposed(dict):
    '''
    Class representing Gene Matrix Transposed (GMT) format 
    
    * Tab delimited file describing gene sets. 
    * Each row represents a gene set
    * Each gene set is described by a name, a description, 
      and the genes in the gene set.
    
    * Example below: 
    ---------------------
    NCI_A6B1_A6B4_INTEGRIN_PATHWAY	NCI: Integrin signaling	192897	...
    NCI_AJDISS_2PATHWAY  NCI: junction stability	17295	14573	...
    NCI_ALK1_2PATHWAY	NCI: ALK1 pathway	11477	11482	14225   ...
    ... 

    '''
    def __init__(self, gene_matrix=(), gmt_file=None):
        super(GeneMatrixTransposed, self).__init__(gene_matrix) 
        if gmt_file is None and hasattr(gene_matrix, 'gmt_file'): 
            gmt_file = gene_matrix.gmt_file
        self.gmt_file = gmt_file 

    def __repr__(self): 
        return super(GeneMatrixTransposed, self).__repr__()

    ''' print command will give the GMT file output ''' 
    def __str__(self):
        return ("\n".join([str(self[x]) for x in  self.keys()]))   

    
    @classmethod
    def read_gmt_file(cls, input_file):
        ''' Generates GeneMatrix from GMT input_file '''
        with open(input_file, 'r') as input_fid:
            lines = map(lambda x: x.strip(), input_fid.readlines()) 
            gene_sets = map(lambda x: GeneSet.parse_gene_set_str(x), lines) 
            gene_matrix = GeneMatrixTransposed() 
            for x in gene_sets: 
                gene_matrix[x.name] = x
            gene_matrix.gmt_file = input_file
            return gene_matrix

    def write_gmt_file(self, output_file): 
        ''' Writes gene_matrix to GMT output_file '''
        with open(output_file, 'w') as fid: 
            fid.write(self.__str__()) 

    def write_output_file(self, output_file): 
        with open(output_file, 'w') as fid: 
            for name in self.keys(): 
                line = "%s\t%d" % (name, len(self[name])) 
                line = line + "\t" + "\t".join(list(self[name])) + "\n" 
                fid.write(line)
    
    def compute_overlap(self, names=None): 
        ''' 
        Returns overlap among different GeneSets in GMT file, computed as
        
        $overlap(S_a, S_b) = intersection(S_a, S_b) / union(S_a, S_b)$ 

        where S_a and S_b are two GeneSets
        '''
        if names is None: 
            names = self.keys() 
        overlap_matrix = [ [ self[x].compute_overlap(self[y]) for x in names ] for y in names ]            
        return overlap_matrix 
    
    def write_overlap_file(self, output_file, names=None): 
        ''' 
        Writes the overlap file among the GeneSets in "names" 

        @argument output_file: Where to save overlap 
        @argument names: Which GeneSets to compute overlap among (default: all of them)  
        ''' 
        if names is None: 
            names = self.keys() 
        with open(output_file, 'w') as fid: 
            first_line = "\t" + "\t".join(names) + "\n"
            fid.write(first_line) 
            for name in  names: 
                curr_line = name 
                for name2 in names: 
                    curr_line = curr_line + "\t" + str(self[name].compute_overlap(self[name2])) 
                curr_line = curr_line + "\n" 
                fid.write(curr_line) 
    
    def get_adjacency_list(self, names = None, threshold=0, get_connected=False): 
        if names is None: 
            names = self.keys() 
        overlap_matrix = self.compute_overlap(names) 
        n = len(names)
        edges = [(overlap_matrix[i][j], i, j) for i in range(n) for j in range(i+1, n)]
        threshold_edges = [(w, i, j) for (w, i, j) in edges if w >= float(threshold)] 
        print >> sys.stderr, "%d edges out of %d above threshold %s" % (len(threshold_edges), len(edges), threshold) 
        discarded_edges = [(w, i, j) for (w, i, j) in edges if w < threshold] 
        discarded_edges.sort(reverse = True) 
        new_edges_added = []
        max_edges = []
        if get_connected: 
            from UnionFind import UnionFind
            subtrees = UnionFind()
            tree = []
            num_edges = 0 
            for w,i,j in threshold_edges:
                # keep merging subtrees till can't find edges or all relevant edges have been added. 
                if subtrees[i] != subtrees[j]:
                    tree.append((i,j))
                    subtrees.union(i,j)
                    num_edges = num_edges + 1 
                if (num_edges == (n-1)):
                    break 
            if (num_edges < (n-1)): 
                print >> sys.stderr, 'graph not connected - computing maximum spanning tree over remaining edges' 
                # Graph not connected.
                num_edges = 0 
                new_edges_added = []
                for w,i,j in discarded_edges:
                    # keep merging subtrees till can't find edges or all relevant edges have been added. 
                    if subtrees[i] != subtrees[j]:
                        tree.append((i,j))
                        subtrees.union(i,j)
                        # print 'Adding extra edge (%f, %d, %d)' % (w, i, j)
                        new_edges_added.append((w, i, j))
                        num_edges = num_edges + 1 
                    if num_edges == (n-1):
                        break 
                print >> sys.stderr, 'Added %d new edges' % (len(new_edges_added))
        return (threshold_edges, new_edges_added)

    def write_adjacency_list(self, output_file, threshold, get_connected):
        names = self.keys() 
        (adjacency_list, other_edges) = self.get_adjacency_list(names, threshold, get_connected)
        with open(output_file, 'w') as fid: 
            for w, i, j in adjacency_list: 
                fid.write('%s (gg) %s = %f\n' % (names[i], names[j], w))
            for w, i, j in other_edges: 
                fid.write('%s (gg) %s = %f\n' % (names[i], names[j], w))

    
    
